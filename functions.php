<?php

// add_action('wp_head', function () {
//     die('salut les gens');
// });

namespace App; // On peut rajouter une NAMESPACE, mais pour qu'il n'y ait pas d'erreur, la balise <?php doit être tout en haut

add_theme_support('title-tag');

add_action('after_setup_theme', function () { //J'ajoute une action (  add_theme_support('title-tag')  ) qui se déclenchera lorsque le theme est lancé (after_setup_theme)
    add_theme_support('title-tag');
});

// ON PEUT AUSSI CRÉER LA FONCTION ET L'APPELER APRÈS
function montheme_supports()
{
    add_theme_support('title-tag');
}
add_action('after_setup_theme', 'App\montheme_supports'); // --> Ceci est un HOOK

function montheme_register_assets()
{
    wp_register_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
    wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', ['popper', 'jquery']); // On dit à WP que le JS a besoin (dépend) de popper et jquery pour fonctionner...
    wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js');
    wp_deregister_script('jquery'); // Annule l'enregistrement de la version de jquery de WP pour que je puisse utiliser celle déjà présente sur mon ordinateur
    wp_register_script('jquery', 'https://code.jquery.com/jquery-3.3.1.slim.min.js');
    wp_enqueue_style('bootstrap');
    wp_enqueue_script('bootstrap'); // ...alors on a seulement besoin de charger Bootstrap qui va d'abord charger popper et jquery
}
add_action('wp_enqueue_scripts', 'App\montheme_register_assets');
